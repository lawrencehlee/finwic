﻿using System;
using Finwic.Analytics;
using Finwic.Filesystem;
using Microsoft.Extensions.DependencyInjection;

namespace Finwic
{
    class Program
    {
        public static void Main(string[] args)
        {
            IServiceCollection services = ConfigureServices();
            var serviceProvider = services.BuildServiceProvider();
            if (args.Length != 1)
            {
                throw new ArgumentException("Base directory required");
            }
            serviceProvider.GetService<App>().Run(args[0]);
        }

        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddSingleton<FilenameAnalyzer>();
            services.AddSingleton<FilenameReader>();
            services.AddSingleton<WordCloudGenerator>();
            services.AddSingleton<ITokenSplitStrategy, UpperCamelCaseStrategy>();
            services.AddSingleton<ITokenSplitStrategy, LowerCamelCaseStrategy>();
            services.AddSingleton<ITokenSplitStrategy, SnakeCaseStrategy>();
            services.AddTransient<App>();
            return services;
        }
    }
}
