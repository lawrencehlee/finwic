using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using Finwic.Analytics;
using Finwic.Filesystem;
using WordCloudSharp;

namespace Finwic
{
    public class App
    {
        private FilenameReader Reader { get; }
        private FilenameAnalyzer Analyzer { get; }
        private WordCloudGenerator WordCloudGenerator { get; }

        public App(FilenameReader reader, FilenameAnalyzer analyzer, WordCloudGenerator wordCloudGenerator)
        {
            Reader = reader;
            Analyzer = analyzer;
            WordCloudGenerator = wordCloudGenerator;
        }

        public void Run(string baseDirectory)
        {
            IEnumerable<string> filenames = Reader.Read(new FilenameReadCommand(baseDirectory));
            IOrderedEnumerable<KeyValuePair<string, int>> analyzed = Analyzer
                .Analyze(filenames)
                .OrderByDescending(pair => pair.Value);

            var words = new List<string>();
            var frequencies = new List<int>();
            foreach ((string word, int frequency) in analyzed)
            {
                words.Add(word);
                frequencies.Add(frequency);
            }

            WordCloudGenerator.GenerateAndSave(words, frequencies);
        }
    }
}