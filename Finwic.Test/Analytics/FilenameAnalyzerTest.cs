using Xunit;
using Moq;
using Finwic.Analytics;
using System.Collections.Generic;

namespace Finwic.Test
{
    public class FilenameAnalyzerTest
    {
        private readonly Mock<ITokenSplitStrategy> strategy1 = new Mock<ITokenSplitStrategy>();
        private readonly Mock<ITokenSplitStrategy> strategy2 = new Mock<ITokenSplitStrategy>();
        private readonly FilenameAnalyzer instance;

        public FilenameAnalyzerTest()
        {
            instance = new FilenameAnalyzer(new List<ITokenSplitStrategy> { strategy1.Object, strategy2.Object });
        }

        [Fact]
        public void CallsAllStrategiesOnEachFilename()
        {
            var filenames = new List<string> { "k", "np" };
            strategy1.Setup(strategy => strategy.Split(It.IsAny<string>())).Returns(new TokenSplitStrategyResult());
            strategy2.Setup(strategy => strategy.Split(It.IsAny<string>())).Returns(new TokenSplitStrategyResult());

            instance.Analyze(filenames);

            strategy1.Verify(strategy => strategy.Split("k"));
            strategy1.Verify(strategy => strategy.Split("np"));
            strategy2.Verify(strategy => strategy.Split("k"));
            strategy2.Verify(strategy => strategy.Split("np"));
        }

        [Fact]
        public void ReturnsTokensWithMaxScoreAsKeys()
        {
            var filenames = new List<string> { "k", "np" };
            strategy1.Setup(strategy => strategy.Split("k")).Returns(
                new TokenSplitStrategyResult { Score = 0.5, Tokens = new List<string> { "strategy1-high" } });
            strategy1.Setup(strategy => strategy.Split("np")).Returns(
                new TokenSplitStrategyResult { Score = 0.25, Tokens = new List<string> { "strategy1-low" } });
            strategy2.Setup(strategy => strategy.Split("k")).Returns(
                new TokenSplitStrategyResult { Score = 0.25, Tokens = new List<string> { "strategy2-low" } });
            strategy2.Setup(strategy => strategy.Split("np")).Returns(
                new TokenSplitStrategyResult { Score = 0.5, Tokens = new List<string> { "strategy2-high" } });

            Dictionary<string, int> actual = instance.Analyze(filenames);

            Assert.Equal(2, actual.Keys.Count);
            Assert.True(actual.ContainsKey("strategy1-high"));
            Assert.True(actual.ContainsKey("strategy2-high"));
        }

        [Fact]
        public void ReturnsTokensMappedToFrequency()
        {
            var filenames = new List<string> { "k", "np" };
            strategy1.Setup(strategy => strategy.Split("k")).Returns(
                new TokenSplitStrategyResult { Score = 0.5, Tokens = new List<string> { "count2", "count1", "count3" } });
            strategy1.Setup(strategy => strategy.Split("np")).Returns(new TokenSplitStrategyResult { Score = 0.25 });
            strategy2.Setup(strategy => strategy.Split("k")).Returns(new TokenSplitStrategyResult { Score = 0.25 });
            strategy2.Setup(strategy => strategy.Split("np")).Returns(
                new TokenSplitStrategyResult { Score = 0.5, Tokens = new List<string> { "count2", "count3", "count3" } });

            Dictionary<string, int> actual = instance.Analyze(filenames);

            Assert.Equal(3, actual.Keys.Count);
            Assert.Equal(1, actual["count1"]);
            Assert.Equal(2, actual["count2"]);
            Assert.Equal(3, actual["count3"]);
        }
    }
}