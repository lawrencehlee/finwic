# Finwic (Filename Word Cloud)

Generates a word cloud based on files in the specified directory. Will respect top-level .gitignore files but not subdirectory ones.

Usage:

    dotnet run --project Finwic <base-directory>

Non-Windows users may need to install some native dependencies:

    sudo apt install libc6-dev
    sudo apt install libgdiplus

