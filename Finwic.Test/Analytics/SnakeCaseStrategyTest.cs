using System.Collections.Generic;
using System.Linq;
using Finwic.Analytics;
using Xunit;

namespace Finwic.Test.Analytics
{
    public class SnakeCaseStrategyTest
    {
        private readonly SnakeCaseStrategy _instance = new SnakeCaseStrategy();

        [Fact]
        public void GivenSymbolIsEmpty_ThenReturnsZeroScoreAndNoTokens()
        {
            TokenSplitStrategyResult actual = _instance.Split("");
            Assert.Equal(0, actual.Score);
            Assert.True(actual.Tokens.Count == 0);
        }

        [Fact]
        public void GivenSymbolIsInSnakeCase_ThenReturnsOneScoreAndCorrectlySplitTokens()
        {
            TokenSplitStrategyResult actual = _instance.Split("sneky_snek_snok");
            Assert.Equal(1, actual.Score);
            Assert.Equal(new List<string> {"sneky", "snek", "snok"}, actual.Tokens);
        }

        [Fact]
        public void GivenSymbolHasNonUnderscoreSpecialCharacters_ThenReturnsLoweredScoreScaledToNumberOfInvalidCharacters()
        {
            TokenSplitStrategyResult few = _instance.Split("sneky-snek_snok");
            TokenSplitStrategyResult many = _instance.Split("sneky-snek-snok");
            
            Assert.True(few.Score < 1);
            Assert.True(few.Score > many.Score);
            
            Assert.Equal(new List<string> {"sneky-snek", "snok"}, few.Tokens);
            Assert.Equal(new List<string> {"sneky-snek-snok"}, many.Tokens);
        }

        [Fact]
        public void GivenSymbolHasUppercaseCharacters_ThenReturnsLoweredScoreScaledToNumberOfUppercaseCharacters()
        {
            TokenSplitStrategyResult few = _instance.Split("sneky_sneK_snok");
            TokenSplitStrategyResult many = _instance.Split("sneky_snEK_snoK");
            
            Assert.True(few.Score < 1);
            Assert.True(few.Score > many.Score);
            
            Assert.Equal(new List<string> {"sneky", "snek", "snok"}, few.Tokens);
            Assert.Equal(new List<string> {"sneky", "snek", "snok"}, many.Tokens);
            
        }
    }
}