using System;
using System.Collections.Generic;
using System.Linq;

namespace Finwic.Analytics
{
    public class LowerCamelCaseStrategy : CamelCaseStrategy
    {
        public override TokenSplitStrategyResult Split(string symbol)
        {
            if (symbol.Length == 0)
            {
                return new TokenSplitStrategyResult();
            }

            if (symbol.ToLower() == symbol)
            {
                return new TokenSplitStrategyResult
                {
                    Score = 1,
                    Tokens = new List<string> {symbol}
                };
            }

            var oddities = 0;
            if (char.IsUpper(symbol[0]))
            {
                oddities++;
            }

            oddities += symbol.Count(c => !char.IsLetterOrDigit(c));

            List<string> tokens = SplitIntoTokens(symbol);
            double score = (double) (tokens.Count - oddities) / tokens.Count;
            if (score < 0)
            {
                score = 0;
            }

            return new TokenSplitStrategyResult
            {
                Tokens = tokens,
                Score = score
            };
        }
    }
}