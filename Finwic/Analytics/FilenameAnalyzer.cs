using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Finwic.Analytics
{
    /// <summary>
    /// Analyzes names for word frequency
    /// </summary>
    public class FilenameAnalyzer
    {
        private IEnumerable<ITokenSplitStrategy> Strategies { get; }
        
        public FilenameAnalyzer(IEnumerable<ITokenSplitStrategy> strategies)
        {
            Strategies = strategies;
        }

        protected FilenameAnalyzer()
        {
            Strategies = new List<ITokenSplitStrategy>();
        }

        public virtual Dictionary<string, int> Analyze(IEnumerable<string> filenames)
        {
            // Split and aggregate into tokens based on the highest-scored strategy
            IEnumerable<string> tokens = filenames
                .Select(filename => Strategies
                    .Select(strategy => strategy.Split(filename))
                    .OrderByDescending(result => result.Score)
                    .First())
                .SelectMany(result => result.Tokens);
            return tokens
                .GroupBy(token => token)
                .ToDictionary(grouping => grouping.Key, grouping => grouping.Count());
        }
    }
}