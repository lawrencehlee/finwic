using System.Collections.Generic;
using System.Linq;

namespace Finwic.Analytics
{
    public abstract class CamelCaseStrategy : ITokenSplitStrategy
    {
        public abstract TokenSplitStrategyResult Split(string symbol);

        protected static List<string> SplitIntoTokens(string symbol)
        {
            List<int> upperIndexes = new List<int>();
            List<string> tokens = new List<string>();

            for (var i = 0; i < symbol.Length; ++i)
            {
                if (char.IsUpper(symbol[i]))
                {
                    upperIndexes.Add(i);
                }
            }

            var lastUpperIndex = 0;
            foreach (int upperIndex in upperIndexes)
            {
                if (upperIndex > lastUpperIndex)
                {
                    tokens.Add(symbol.Substring(lastUpperIndex, upperIndex - lastUpperIndex));
                }

                lastUpperIndex = upperIndex;
            }

            tokens.Add(symbol.Substring(lastUpperIndex));

            tokens = tokens
                .Select(token => token.Trim().ToLower())
                .Where(token => token.Length > 0)
                .ToList();

            // Adjust for acronyms (adjacent uppercase letters)
            List<string> adjusted = new List<string>();
            string currentAcronym = "";
            foreach (string token in tokens)
            {
                if (token.Length == 1)
                {
                    currentAcronym += token;
                }
                else if (currentAcronym.Length > 0)
                {
                    adjusted.Add(currentAcronym);
                    adjusted.Add(token);
                    currentAcronym = "";
                }
                else
                {
                    adjusted.Add(token);
                }
            }

            if (currentAcronym.Length > 0)
            {
                adjusted.Add(currentAcronym);
            }

            return adjusted;
        }
    }
}