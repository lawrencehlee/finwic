using System.Collections.Generic;

namespace Finwic.Analytics
{
    /// <summary>
    /// A strategy for splitting a symbol into tokens
    /// </summary>
    public interface ITokenSplitStrategy
    {
        /// <summary>
        /// Split a given symbol into tokens based on some strategy
        /// </summary>
        TokenSplitStrategyResult Split(string symbol);
    }

    public class TokenSplitStrategyResult
    {
        public List<string> Tokens { get; set; } = new List<string>();

        /// <summary>
        /// How confident (from 0 to 1) was the split?
        /// </summary>
        public double Score { get; set; }
    }
}