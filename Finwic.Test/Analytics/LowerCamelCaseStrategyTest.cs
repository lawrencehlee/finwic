using System.Collections.Generic;
using System.Linq;
using Finwic.Analytics;
using Xunit;

namespace Finwic.Test.Analytics
{
    public class LowerCamelCaseStrategyTest
    {
        private readonly LowerCamelCaseStrategy _instance = new LowerCamelCaseStrategy();
        [Fact]
        public void GivenSymbolIsEmpty_ThenReturnsZeroScoreAndNoTokens()
        {
            TokenSplitStrategyResult actual = _instance.Split("");
            Assert.Equal(0, actual.Score);
            Assert.True(actual.Tokens.Count == 0);
        }

        [Fact]
        public void GivenSymbolIsAllLowercase_ThenReturnsOneScoreAndSymbolToken()
        {
            TokenSplitStrategyResult actual = _instance.Split("lowerislower");
            Assert.Equal(1, actual.Score);
            Assert.Equal("lowerislower", actual.Tokens.First());
        }
        
        [Fact]
        public void GivenSymbolIsLowerCamelCase_ThenReturnsOneScoreAndSymbolTokens()
        {
            TokenSplitStrategyResult actual = _instance.Split("lowerIsYupper");
            Assert.Equal(1, actual.Score);
            Assert.Equal(new List<string> { "lower", "is", "yupper" }, actual.Tokens);
        }


        [Fact]
        public void GivenSymbolStartsWithUpperCase_ThenScoresPrettyHigh()
        {
            TokenSplitStrategyResult actual = _instance.Split("UpperIsYupper");
            Assert.True(actual.Score > 0.5 && actual.Score < 1);
            Assert.Equal(new List<string> { "upper", "is", "yupper" }, actual.Tokens);
        }

        [Fact]
        public void GivenSymbolWithWeirdCharacters_ThenScoresInverselyProportionallyToNumberOfWeirdCharacters()
        {
            TokenSplitStrategyResult few = _instance.Split("weird_AsBalloonsGo");
            TokenSplitStrategyResult some = _instance.Split("weird_As-BalloonsGo");
            TokenSplitStrategyResult many = _instance.Split("Weird_As-Balloons ??? @@Go@))@)");
            Assert.True(few.Score < 1);
            Assert.True(some.Score < few.Score);
            Assert.True(many.Score < some.Score);

            Assert.Equal(new List<string> { "weird_", "as", "balloons", "go" }, few.Tokens);
            Assert.Equal(new List<string> { "weird_", "as-", "balloons", "go" }, some.Tokens);
            Assert.Equal(new List<string> { "weird_", "as-", "balloons ??? @@", "go@))@)" }, many.Tokens);
        }

        [Fact]
        public void GivenSymbolWithAcronyms_ThenSplitsTokenCorrectly()
        {
            TokenSplitStrategyResult actual = _instance.Split("isCNNTelevision");
            Assert.Equal(1, actual.Score);
            Assert.Equal(new List<string> { "is", "cnn", "television" }, actual.Tokens);
        }

        [Fact]
        public void RandomEdgeCases()
        {
            Dictionary<string, List<string>> expected = new Dictionary<string, List<string>>
            {
                { "whatBoutI", new List<string> { "what", "bout", "i" } },
                { "iKnow", new List<string> { "i", "know" } },
                { "ok_but_how", new List<string> { "ok_but_how" } },
                { "Yep", new List<string> { "yep" } },
                { "yep", new List<string> { "yep" } }
            };

            foreach ((string input, List<string> tokens) in expected)
            {
                TokenSplitStrategyResult actual = _instance.Split(input);
                Assert.Equal(tokens, actual.Tokens);
                Assert.True(actual.Score >= 0 && actual.Score <= 1);
            }
        }
    }
}