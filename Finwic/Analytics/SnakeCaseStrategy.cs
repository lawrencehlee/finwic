using System;
using System.Collections.Generic;
using System.Linq;

namespace Finwic.Analytics
{
    public class SnakeCaseStrategy : ITokenSplitStrategy
    {
        public TokenSplitStrategyResult Split(string symbol)
        {
            if (symbol.Length == 0)
            {
                return new TokenSplitStrategyResult();
            }

            int oddities = symbol.ToCharArray().Count(c =>
                char.IsUpper(c) ||
                (c != '_' && !char.IsLetterOrDigit(c)));
            List<string> tokens = symbol.Split("_")
                .Where(t => !string.IsNullOrWhiteSpace(t))
                .Select(t => t.Trim().ToLower())
                .ToList();
            
            return new TokenSplitStrategyResult
            {
                Score = (double) (tokens.Count - oddities) / tokens.Count,
                Tokens = tokens
            };
        }
    }
}