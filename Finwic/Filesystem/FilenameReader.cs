using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MAB.DotIgnore;

namespace Finwic.Filesystem
{
    /// <summary>
    /// Given a base directory, reads all filenames under that directory
    /// </summary>
    public class FilenameReader
    {
        public FilenameReader()
        {
        }

        public virtual IEnumerable<string> Read(FilenameReadCommand command)
        {
            string fullBaseDirectory = Path.GetFullPath(command.BaseDirectory) ??
                                       throw new ArgumentException("Could not evaluate directory");
            IEnumerable<string> filepaths = Directory
                .EnumerateFiles(fullBaseDirectory, "*", SearchOption.AllDirectories)
                // Turn into path relative to base directory
                .Select(filepath => filepath.Replace(fullBaseDirectory, ""))
                // Delete the leading slash if present
                .Select(filepath => filepath[0] == '/' || filepath[0] == '\\' ? filepath.Substring(1) : filepath);

            var ignores = new IgnoreList(Path.Combine(command.BaseDirectory, ".gitignore"));
            ignores.AddRule(".git/");

            foreach (var filepath in filepaths)
            {
                if (ignores.IsIgnored(filepath, false)) continue;
                int lastIndexOfSlash = filepath.LastIndexOfAny(new[] {'/', '\\'});
                string filename = lastIndexOfSlash == -1 ? filepath : filepath.Substring(lastIndexOfSlash + 1);
                string[] splitByDot = filename.Split('.', StringSplitOptions.RemoveEmptyEntries);

                for (var i = 0; i < splitByDot.Length; ++i)
                {
                    // Only 1 split => just return it (e.g. a .env file)
                    // Otherwise ignore the last split, since it's most likely the extension
                    if (i == 0 || i < splitByDot.Length - 1)
                    {
                        yield return splitByDot[i];
                    }
                }
            }
        }
    }

    public class FilenameReadCommand
    {
        public string BaseDirectory { get; }

        public FilenameReadCommand(string baseDirectory)
        {
            BaseDirectory = baseDirectory;
        }
    }
}