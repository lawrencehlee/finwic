using System;
using System.Collections.Generic;
using System.Linq;

namespace Finwic.Analytics
{
    /// <summary>
    /// Splits tokens assuming they're in upper camel case, e.g. MyClassFoo.
    /// Acronyms should follow this, e.g. CiaIsWatching.
    /// </summary>
    public class UpperCamelCaseStrategy : CamelCaseStrategy
    {
        public override TokenSplitStrategyResult Split(string symbol)
        {
            if (symbol.Length == 0)
            {
                return new TokenSplitStrategyResult();
            }

            if (symbol.ToLower() == symbol)
            {
                return new TokenSplitStrategyResult
                {
                    Score = 0,
                    Tokens = new List<string> {symbol}
                };
            }

            var oddities = 0;
            if (char.IsLower(symbol[0]))
            {
                oddities++;
            }

            oddities += symbol.Count(c => !char.IsLetterOrDigit(c));

            List<string> tokens = SplitIntoTokens(symbol);
            double score = (double) (tokens.Count - oddities) / tokens.Count;
            if (score < 0)
            {
                score = 0;
            }

            return new TokenSplitStrategyResult
            {
                Tokens = tokens,
                Score = score
            };
        }
    }
}