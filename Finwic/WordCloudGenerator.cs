using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using WordCloudSharp;

namespace Finwic
{
    public class WordCloudGenerator
    {
        public void GenerateAndSave(List<string> words, List<int> frequencies)
        {
            var wordCloudGenerator = new WordCloud(1024, 768, fontname: "Open Sans");
            Image image = wordCloudGenerator.Draw(words, frequencies);
            EncoderParameters encoderParameters = new EncoderParameters(1);
            EncoderParameter encoderParameter = new EncoderParameter(Encoder.Quality, 100L);
            encoderParameters.Param[0] = encoderParameter;
            image.Save("word-cloud.jpg", GetEncoderInfo("image/jpeg"), encoderParameters);
        }
        
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] encoders = ImageCodecInfo.GetImageEncoders();
            return encoders.First(encoder => encoder.MimeType == mimeType);
        }
    }
}